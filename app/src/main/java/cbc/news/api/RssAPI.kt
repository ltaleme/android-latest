package cbc.news.api

import cbc.news.model.RSSFeed
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by teferi on 2017-09-28.
 */

const val API_URL = "http://www.cbc.ca/cmlink/rss-topstories/"

interface RssAPI {

    @GET(".")
    fun loadRSSFeed(): Single<RSSFeed>
}