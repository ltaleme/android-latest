package cbc.news.ui.abstracts

import android.os.Bundle
import android.os.Handler
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable

//Created by teferi on 21/10/18

abstract class BaseActivity : DaggerAppCompatActivity() {

    protected val compositeDisposable by lazy { CompositeDisposable() }

    protected val handler: Handler by lazy { Handler() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
        initUI()
        observeViewModelLiveData()
    }

    protected abstract fun initUI()

    protected abstract fun observeViewModelLiveData()

    protected abstract fun layoutId(): Int

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
        handler.removeCallbacksAndMessages(null)
    }
}