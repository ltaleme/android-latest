package cbc.news.ui.abstracts

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable


//Created by teferi on 21/10/18

abstract class BaseFragment : DaggerFragment() {

    protected val compositeDisposable by lazy { CompositeDisposable() }

    protected val handler: Handler by lazy { Handler() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeViewModelLiveData()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
        handler.removeCallbacksAndMessages(null)
    }

    protected abstract fun layoutId(): Int

    protected abstract fun initUI()

    protected abstract fun observeViewModelLiveData()

}