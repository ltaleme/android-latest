package cbc.news.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import cbc.news.R
import cbc.news.util.invisible
import cbc.news.util.visible
import cbc.news.ui.abstracts.BaseFragment
import cbc.news.viewmodel.DetailViewModel
import kotlinx.android.synthetic.main.fragment_detail.*
import javax.inject.Inject


/**
 * Created by teferi on 2017-09-24.
 */

class DetailFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: DetailViewModel

    companion object {
        private const val EXTRA_LINK_URL = "EXTRA_LINK_URL"

        fun newInstance(linkUrl: String) = DetailFragment().apply { arguments = Bundle().apply { putString(EXTRA_LINK_URL, linkUrl) } }
    }

    override fun initUI() {
        web_view.tag = arguments?.getString(EXTRA_LINK_URL)
        viewModel.loadUrl(web_view)
    }

    override fun observeViewModelLiveData() {
        viewModel.loadUrlLiveData.observe(this, Observer {
            web_view?.visible()
            progress_bar?.invisible()
        })
    }

    override fun layoutId() = R.layout.fragment_detail


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        web_view.saveState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        web_view.restoreState(savedInstanceState)
    }

}


