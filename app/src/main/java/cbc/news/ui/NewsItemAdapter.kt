package cbc.news.ui


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cbc.news.R
import cbc.news.util.loadUrl
import cbc.news.model.Channel
import kotlinx.android.synthetic.main.news_item.view.*
import android.support.v7.widget.RecyclerView


/**
 * Created by teferi on 2017-09-24.
 */

class NewsItemAdapter(private val newsItems: List<Channel.NewsItem>,
                      private val listener: ItemClickListener) : RecyclerView.Adapter<NewsItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = NewsItemAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
            = holder.bind(newsItems[position], listener)

    override fun getItemCount() = newsItems.size

    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(newsItem: Channel.NewsItem, listener: ItemClickListener) = with(view) {
            title_text_view.text = newsItem.title
            publication_date_text_view.text = newsItem.pubDate
            author_text_view.text = newsItem.author
            image_view.loadUrl(newsItem.imageUrl)
            container.setOnClickListener { listener.onItemClick(newsItem) }
        }
    }

    interface ItemClickListener {
        fun onItemClick(newsItem: Channel.NewsItem)
    }

}