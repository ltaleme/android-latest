package cbc.news.ui

import android.arch.lifecycle.Observer
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import cbc.news.R
import cbc.news.model.Channel
import cbc.news.model.RSSFeed
import cbc.news.ui.abstracts.BaseActivity
import cbc.news.util.*
import cbc.news.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject

class MainActivity : BaseActivity() {

    private var newsItemAdapter: NewsItemAdapter? = null

    private var newsItems = mutableListOf<Channel.NewsItem>()

    @Inject
    lateinit var viewModel: MainViewModel

    override fun initUI() {
        recycler_view.setHasFixedSize(true)
        recycler_view.layoutManager = LinearLayoutManager(this)

        newsItemAdapter = NewsItemAdapter(newsItems, object : NewsItemAdapter.ItemClickListener {
            override fun onItemClick(newsItem: Channel.NewsItem) {
                 showFragment(DetailFragment.newInstance(newsItem.link))
            }
        })

        recycler_view.adapter = newsItemAdapter

        viewModel.loadRssFeed()
    }

    override fun observeViewModelLiveData() {

        viewModel.rssFeedData.doWhenObserve(this,
                onProgress = {
                    progress_bar.visible()
                },
                onSuccess = {
                    progress_bar.invisible()
                    recycler_view.visible()

                    it?.let {
                        (it as RSSFeed).let {
                            this.newsItems.clear()
                            this.newsItems.addAll(it.channel.itemList.filter { it.isEmpty })

                            newsItemAdapter?.notifyDataSetChanged()
                        }
                    }

                },
                onError = {
                    progress_bar.invisible()
                    it?.let { Toast.makeText(this, it.localizedMessage, Toast.LENGTH_LONG).show() }
                })
    }

    override fun layoutId() = R.layout.activity_home

}
