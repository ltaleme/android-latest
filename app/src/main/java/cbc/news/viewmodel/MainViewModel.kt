package cbc.news.viewmodel



import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

import cbc.news.repo.CBCRepository
import cbc.news.util.LDStatus
import cbc.news.util.SchedulerProvider
import javax.inject.Inject

/**
 * Created by teferi on 2018-07-13.
 */

class MainViewModel @Inject constructor(private val schedulerProvider: SchedulerProvider,
                                        private val cbcRepository: CBCRepository) : ViewModel() {


    private val _rssFeedData = MutableLiveData<Pair<LDStatus, Any?>>()
    val rssFeedData : LiveData<Pair<LDStatus, Any?>>
        get() = _rssFeedData


    fun loadRssFeed() {
        _rssFeedData.value = Pair(LDStatus.PROGRESS, null)
        cbcRepository.loadRssFeed()
                .compose(schedulerProvider.getSchedulersForSingle())
                .subscribe { rssFeed, error ->
                    if (rssFeed != null) {
                        _rssFeedData.value =  Pair(LDStatus.SUCCESS, rssFeed)
                    } else {
                        _rssFeedData.value = Pair(LDStatus.ERROR, error)
                    }
                }
    }


}
