package cbc.news.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.webkit.WebView
import android.webkit.WebViewClient
import cbc.news.util.SchedulerProvider
import javax.inject.Inject

//Created by teferi on 03/11/18

class DetailViewModel @Inject constructor(private val schedulerProvider: SchedulerProvider): ViewModel(){

    val loadUrlLiveData = MutableLiveData<String>()

    fun loadUrl(webView: WebView){

        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                loadUrlLiveData.value = url
            }
        }

        webView.apply { loadUrl(tag as String) }

    }
}