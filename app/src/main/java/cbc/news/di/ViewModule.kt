package cbc.news.di

import cbc.news.ui.DetailFragment
import cbc.news.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

//Created by teferi on 20/10/18

@Module
abstract class ViewModule {

    @ContributesAndroidInjector(modules = [NewsListViewModelModule::class])
    abstract fun bindHomeActivity(): MainActivity

    @ContributesAndroidInjector(modules = [DetailViewModelModule::class])
    abstract fun bindDetailFragment(): DetailFragment

}