package cbc.news.di

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import cbc.news.api.API_URL
import cbc.news.api.RssAPI
import cbc.news.db.AppDatabase
import cbc.news.db.UserDao
import cbc.news.repo.CBCRepository
import cbc.news.util.SchedulerProvider
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import javax.inject.Singleton

//Created by teferi on 20/10/18

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context = application.applicationContext

    @Provides
    @Singleton
    fun provideSchedulerProvider() = SchedulerProvider(Schedulers.io(), AndroidSchedulers.mainThread())

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient.Builder().addInterceptor(loggingInterceptor).build()
    }

    @Provides
    @Singleton
    fun provideRssApi(okHttpClient: OkHttpClient): RssAPI {
        return Retrofit.Builder().baseUrl(API_URL).
                addConverterFactory(SimpleXmlConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

                .client(okHttpClient).build().create(RssAPI::class.java)
    }


    @Provides
    @Singleton
    fun provideUserDAO(application: Application) = Room.databaseBuilder(
            application,
            AppDatabase::class.java, "database-name"
    ).allowMainThreadQueries().build().userDao()

    @Provides
    @Singleton
    fun provideCBCRepository(rssAPI: RssAPI, userDao : UserDao) = CBCRepository(rssAPI, userDao)


}