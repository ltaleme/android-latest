package cbc.news.di

import android.app.Application
import cbc.news.CBCApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

//Created by teferi on 20/10/18

@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class, ViewModule::class])
interface AppComponent : AndroidInjector<CBCApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    override fun inject(application: CBCApp)
}
