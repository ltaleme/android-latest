package cbc.news.di

import cbc.news.repo.CBCRepository
import cbc.news.util.SchedulerProvider
import cbc.news.viewmodel.DetailViewModel
import cbc.news.viewmodel.MainViewModel
import dagger.Module
import dagger.Provides

//Created by teferi on 20/10/18

@Module
class NewsListViewModelModule {
    @Provides
    fun provideNewsListViewModel(schedulerProvider: SchedulerProvider, cbcRepository: CBCRepository) = MainViewModel(schedulerProvider, cbcRepository)
}


@Module
class DetailViewModelModule {
    @Provides
    fun provideDetailViewModel(schedulerProvider: SchedulerProvider) = DetailViewModel(schedulerProvider)
}