package cbc.news

import cbc.news.di.DaggerAppComponent
import dagger.android.DaggerApplication
import timber.log.Timber


/**
 * Created by teferi on 2017-09-24.
 */

class CBCApp : DaggerApplication() {

    private val applicationInjector = DaggerAppComponent.builder().application(this).build()

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }

    override fun applicationInjector() = applicationInjector

}