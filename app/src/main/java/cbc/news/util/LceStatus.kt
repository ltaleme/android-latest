package cbc.news.util


enum class LDStatus {
    PROGRESS,
    ERROR,
    SUCCESS
}