package cbc.news.util

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.view.View
import android.widget.ImageView
import android.support.v4.app.Fragment
import cbc.news.R
import cbc.news.ui.MainActivity
import com.squareup.picasso.Picasso
import io.reactivex.disposables.CompositeDisposable


// Created by teferi on 13/07/18.

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun ImageView.loadUrl(url: String?) = Picasso.with(context).load(url).into(this)

fun LiveData<Pair<LDStatus, Any?>>.doWhenObserve(owner: LifecycleOwner,
                                                 onProgress: (() -> Unit)? = null,
                                                 onSuccess: ((Any?) -> Unit)? = null,
                                                 onError: ((Throwable?) -> Unit)? = null) {
    this.observe(owner, Observer { result ->
        result?.let {
            when (it.first) {
                LDStatus.PROGRESS -> onProgress?.invoke()
                LDStatus.SUCCESS -> onSuccess?.invoke(it.second)
                LDStatus.ERROR -> onError?.invoke(it.second?.let { any ->  any as Throwable })
            }
        }

    })
}

fun MainActivity.showFragment(fragment: Fragment) {
    supportFragmentManager.beginTransaction()
            .add(R.id.home_fragment_container, fragment)
            .addToBackStack(null)
            .commit()
}