package cbc.news.util

import org.simpleframework.xml.Serializer
import org.simpleframework.xml.core.Persister

import cbc.news.model.Description

/**
 * Created by teferi on 2017-09-24.
 */


object DescriptionParser {
    private const val DESC_START_TAG = "<description>"
    private const val DESC_END_TAG = "</description>"

    private val serializer: Serializer = Persister()

    fun getImageUrl(descriptionString: String?) = try {
        serializer.read(Description::class.java, DESC_START_TAG + descriptionString + DESC_END_TAG).imgUrl
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }
}
