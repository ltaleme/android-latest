package cbc.news.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

//Created by teferi on 11/11/18

@Database(entities = [User::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}