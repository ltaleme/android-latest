package cbc.news.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

//Created by teferi on 11/11/18

@Entity
data class User(
        @PrimaryKey var uid: Int,
        @ColumnInfo(name = "first_name") var firstName: String?,
        @ColumnInfo(name = "last_name") var lastName: String?
)