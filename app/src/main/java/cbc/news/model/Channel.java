package cbc.news.model;

import android.text.TextUtils;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

import java.util.List;

import cbc.news.util.DescriptionParser;

@NamespaceList({
        @Namespace(reference = "http://www.w3.org/2005/Atom", prefix = "atom")
})
@Root(strict = false)
public class Channel {
    @ElementList(entry = "link", inline = true, required = false)
    public List<Link> links;

    @ElementList(name = "item", required = true, inline = true, data = true)
    public List<NewsItem> itemList;


    @Element
    String title;
    @Element
    String language;

    @Element(name = "ttl", required = false)
    int ttl;

    @Element(name = "pubDate", required = false)
    String pubDate;

    @Override
    public String toString() {
        return "Channel{" +
                "links=" + links +
                ", itemList=" + itemList +
                ", title='" + title + '\'' +
                ", language='" + language + '\'' +
                ", ttl=" + ttl +
                ", pubDate='" + pubDate + '\'' +
                '}';
    }

    public static class Link {
        @Attribute(required = false)
        public String href;

        @Attribute(required = false)
        public String rel;

        @Attribute(name = "type", required = false)
        public String contentType;

        @Text(required = false)
        public String link;
    }

    @Root(name = "item", strict = false)
    public static class NewsItem {

        @Element(name = "title", required = true)
        public String title;
        @Element(name = "link", required = true)
        public String link;
        @Element(name = "description", required = true, data = true)
        String description;
        @Element(name = "author", required = false)
        public String author;
        @Element(name = "category", required = false)
        public String category;
        @Element(name = "comments", required = false)
        String comments;
        @Element(name = "enclosure", required = false)
        String enclosure;
        @Element(name = "guid", required = false)
        String guid;
        @Element(name = "pubDate", required = false)
        public String pubDate;
        @Element(name = "source", required = false)
        String source;


        public String getImageUrl() {
            return DescriptionParser.INSTANCE.getImageUrl(description);
        }

        public boolean isEmpty = TextUtils.isEmpty(pubDate) && TextUtils.isEmpty(author);


        @Override
        public String toString() {
            return "NewsItem{" +
                    "title='" + title + '\'' +
                    ", link='" + link + '\'' +
                    ", description='" + description + '\'' +
                    ", author='" + author + '\'' +
                    ", category='" + category + '\'' +
                    ", comments='" + comments + '\'' +
                    ", enclosure='" + enclosure + '\'' +
                    ", guid='" + guid + '\'' +
                    ", pubDate='" + pubDate + '\'' +
                    ", source='" + source + '\'' +
                    '}';
        }
    }
}

