package cbc.news.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "rss", strict = false)
public class RSSFeed {

    @Attribute
    String version;

    @Element(data = true)
    Channel channel;

    public Channel getChannel() {
        return channel;
    }

    @Override
    public String toString() {
        return "RSSFeed{" +
                "version='" + version + '\'' +
                ", channel=" + channel +
                '}';
    }
}
