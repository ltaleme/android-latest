package cbc.news.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * Created by teferi on 2017-09-30.
 */

@Root(name = "description", strict = false)
public class Description {

    @Attribute(name = "src")
    @Path("img")
    public String imgUrl;
}
