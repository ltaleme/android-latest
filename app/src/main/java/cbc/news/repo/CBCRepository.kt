package cbc.news.repo

import android.arch.persistence.room.Room
import cbc.news.CBCApp
import cbc.news.api.RssAPI
import cbc.news.db.AppDatabase
import cbc.news.db.UserDao
import cbc.news.model.RSSFeed
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by teferi on 2018-07-13.
 */


class CBCRepository @Inject constructor(private val rssAPI: RssAPI, private val userDao: UserDao) {

    fun loadRssFeed() : Single<RSSFeed>{
        Timber.d("loadRssFeed: isDBEmpty=${userDao.getAll().isEmpty()}")
       return rssAPI.loadRSSFeed()
    }

}